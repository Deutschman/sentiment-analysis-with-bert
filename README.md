This is a Sentiment Analyzer project. It is described in [my medium post](https://towardsdatascience.com/sentiment-analyzer-with-bert-build-tune-deploy-da84c0f2366d).

Requirenments are:

- tensorflow==1.13.2
- bert-serving-server[http]==1.10.0
- bert-serving-client==1.10.0
- Flask==1.1.2
- flask_cors==3.0.8
- numpy==1.19.0
- pandas==1.0.5
- tensorflow_cpu==2.2.0
- gunicorn==20.0.4


